package week2.assignment.datastructures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import week2.assignment.employee.Employee;

public class DataStructureA {
	public ArrayList<String> sortingNames(ArrayList<Employee> employees) {
		ArrayList<String> sortedNames = new ArrayList<>();
		for(Employee e : employees) {
			sortedNames.add(e.getName());
		}
		Collections.sort(sortedNames);
		return sortedNames;
	}
}
