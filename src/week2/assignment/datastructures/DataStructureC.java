
package week2.assignment.datastructures;

import java.util.ArrayList;
import java.util.TreeMap;

import week2.assignment.employee.Employee;

public class DataStructureC {

	public TreeMap<Integer,Double> monthlySalary(ArrayList<Employee> employees) {
		TreeMap<Integer,Double> salaries = new TreeMap<>();

		try {
			for(Employee e : employees) {
				double d = Math.floor(((double)e.getSalary()/12));
				salaries.put(e.getId(),d);
			}
		}catch(Exception e) {
			System.out.println("An Exception occured " + e.getMessage());
		}
		return salaries;
	}

}
