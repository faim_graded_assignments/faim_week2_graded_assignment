package week2.assignment.datastructures;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import week2.assignment.employee.Employee;

public class DataStructureB {
	public TreeMap<String,Integer> cityNameCount(List<Employee> employees) {
		TreeMap<String,Integer> countingCities= new TreeMap<>();
		for(Employee emp:employees)
		{
			String myCity=emp.getCity();
			if(countingCities.containsKey(myCity))
			{
				int count = countingCities.get(myCity);
				countingCities.replace(myCity, (count + 1));
			}
			else
			{
				countingCities.put(myCity, 1);
			}
		}
		return countingCities;
	}
}