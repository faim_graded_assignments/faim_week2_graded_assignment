package week2.assignment.employee;

public class Employee {
	private int id;
	private String name;
	private int age;
	private int salary;
	private String department;
	private String city;
	
	
	public int getId() {
		return id;
	}
	
	
	public void setId(int id) {
		if(id > 0) {
			this.id = id;
		}else {
			throw new IllegalArgumentException("Given id is not valid");
		}
	}
	
	
	public String getName() {
		return name;
	}
	
	
	public void setName(String name) {
		if(name != null || !name.isEmpty()) {
			this.name = name;
		}else {
			throw new IllegalArgumentException("Name cannot be empty");
		}
	}
	
	
	public int getAge() {
		return age;
	}
	
	
	public void setAge(int age) {
		if(age > 0) {
			this.age = age;
		}else {
			throw new IllegalArgumentException("Age Cannot be less then zero");
		}
	}
	
	
	public int getSalary() {
		return salary;
	}
	
	
	public void setSalary(int salary) {
		if(salary > 0) {
			this.salary = salary;
		}else {
			throw new IllegalArgumentException("Salary cannot be zero");
		}
	}
	
	
	public String getDepartment() {
		return department;
	}
	
	
	public void setDepartment(String department) {
		if(department != null || !department.isEmpty()) {
			this.department = department;
		}else {
			throw new IllegalArgumentException("Department Name cannot be Empty");
		}
	}
	
	
	public String getCity() {
		return city;
	}
	
	
	public void setCity(String city) {
		if(city != null || !city.isEmpty()) {
			this.city = city;
		}else {
			throw new IllegalArgumentException("City Name Cannot be Empty");
		}
	}
}