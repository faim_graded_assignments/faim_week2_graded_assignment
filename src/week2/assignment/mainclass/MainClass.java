package week2.assignment.mainclass;

import java.util.ArrayList;

import week2.assignment.datastructures.DataStructureA;
import week2.assignment.datastructures.DataStructureB;
import week2.assignment.datastructures.DataStructureC;
import week2.assignment.employee.Employee;

public class MainClass {
	public static void main(String[] args) {

		ArrayList<Employee> employees = new ArrayList<>();

		Employee e1 = new Employee();
		Employee e2 = new Employee();
		Employee e3 = new Employee();
		Employee e4 = new Employee();
		Employee e5 = new Employee();

		e1.setId(1);
		e1.setName("Aman");
		e1.setAge(20);
		e1.setSalary(1100000);
		e1.setDepartment("IT");
		e1.setCity("Delhi");

		e2.setId(2);
		e2.setName("Bobby ");
		e2.setAge(22);
		e2.setSalary(500000);
		e2.setDepartment("HR");
		e2.setCity("Bombay");

		e3.setId(3);
		e3.setName("Zoe");
		e3.setAge(20);
		e3.setSalary(750000);
		e3.setDepartment("Admin");
		e3.setCity("Delhi");

		e4.setId(4);
		e4.setName("Smitha");
		e4.setAge(21);
		e4.setSalary(1000000 );
		e4.setDepartment("IT");
		e4.setCity("Chennai");

		e5.setId(5);
		e5.setName("Smitha");
		e5.setAge(24);
		e5.setSalary(12000000);
		e5.setDepartment("HR");
		e5.setCity("Bengaluru");

		employees.add(e1);
		employees.add(e2);
		employees.add(e3);
		employees.add(e4);
		employees.add(e5);

		System.out.println("List of All the Employees: ");
		System.out.println();
		System.out.printf("%-4s%-8s%-6s%-9s%-12s%s","ID" ,"Name","Age","salary","Department","City");
		System.out.println();
		for(Employee e: employees) {
			System.out.printf("%-4s%-8s%-6s%-11s%-10s%s",e.getId(),e.getName(),e.getAge(),e.getSalary(),e.getDepartment(),e.getCity());
			System.out.println();
		}
		System.out.println();

		DataStructureA ob1 = new DataStructureA();
		System.out.println("Names of all employees in the sorted order are: ");
		System.out.println(ob1.sortingNames(employees));

		System.out.println();
		DataStructureB ob2 = new DataStructureB();
		System.out.println("count of employees from each city ");
		System.out.println(ob2.cityNameCount(employees));

		System.out.println();
		DataStructureC ob3= new DataStructureC();
		System.out.println("Monthly Salary of employee along with their id is");
		System.out.println(ob3.monthlySalary(employees));
	}

}
